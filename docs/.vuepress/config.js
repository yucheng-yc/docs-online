const { defaultTheme } = require('vuepress')
const { backToTopPlugin } = require('@vuepress/plugin-back-to-top')
const { sidebar,navbar }  =require('./config/index')

// const { docsearchPlugin } = require('@vuepress/plugin-docsearch')


module.exports = {
  theme: defaultTheme({
    logo: '/images/logo.png',
    logoDark: '/images/logo.png',
    repoLabel:'码云仓库',
    repo: 'https://gitee.com/yucheng-yc', // git仓库链接
    navbar,
    sidebar
  }),
  title: '沉鱼文档中心',
  description: '文档中心',
  head: [['link', { rel: 'icon', href: '/images/logo.png' }]],
  plugins: [
    backToTopPlugin(),
    // docsearchPlugin({
    //   apiKey: '',
    //   indexName: '',
    // }),
  ],
}
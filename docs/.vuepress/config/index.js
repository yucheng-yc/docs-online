module.exports = {
  sidebar: {
    
    '/ui-library/': [
      {
        text: '沉鱼组件库',
        collapsible: true,
        children: ['/ui-library/readme.md', '/ui-library/baseUI.md'],
      },
    ],
    '/links/': [
      {
        text: '学习文档导航',
        collapsible: true,
        children: ['/links/readme.md',
        {
          link: '/links/weixin.md',
          text: '微信收藏链接'
        },
        {
          link: '/links/interviewQuestion.md',
          text: '面试题'
        }
      ],
      },
    ],
  },
  navbar: [
    {
      text: '导航',
      collapsible: true,
      children: [
        {
          text: '工具相关',
          collapsible: true,
          children: [
            {
              text: '沉鱼npm库',
              link: 'http://npm.yucheng.press',
            },
            {
              text: 'code编辑器',
              link:'http://code.yucheng.press'
            },
            {
              text: '花猪图床',
              link:'http://tc.yucheng.press'
            },
            {
              text: '宝塔面板',
              link:'http://www.yucheng.press:1225/baota'
            },
            {
                text: 'tinypng',
                link: 'https://tinypng.com/'
            }
          ]
        },
        {
          text: '文章相关',
          collapsible: true,
          children: [
            {
              text: '面试题',
              link: '/interview/'
            },
            {
              text: 'cf文档中心',
              link: 'http://cf.yucheng.press',
            },
            
            // NavbarGroup
            {
              text: '个人博客',
              link:'http://blog.yucheng.press'
            },
            {
              text: '个人知识库',
              link:'http://study.yucheng.press'
            },
          ]
        },
        {
          text: '地址相关',
          link: '/links/address.md'
        }
      ]
    },
    

    // 字符串 - 页面文件路径
    // '/bar/README.md',
  ]
}